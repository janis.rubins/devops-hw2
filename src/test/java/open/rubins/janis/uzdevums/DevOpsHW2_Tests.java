import org.junit.jupiter.api.Assertions;

public class DevOpsHW2_Tests {
    @org.junit.jupiter.api.Test
    public void addNumbersTest() {
        Assertions.assertEquals(4, DevOpsHW2.square(2));
    }

    @org.junit.jupiter.api.Test
    public void squareTest() {
        Assertions.assertEquals(12, DevOpsHW2.addNumbers(9, 3));
    }
}
